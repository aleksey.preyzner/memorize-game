#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import tkinter
from tkinter import messagebox
from random import shuffle
# TODO стартовое меню к указанием размера сетки
# TODO меню выхода из текущей игры или новой игры с новыми параметрами.


class GameSet(object):

    def __init__(self, n):
        self._n = n
        self.SIDE = 6 + 2*self._n
        self._scores = 0
        self.settings = {
            # задаем размер поля
            'SIDE': self.SIDE,
            'Q_SIDE': self.SIDE**2//2,
            'game_time': self.SIDE * 30,
            'stop_time': False,
            'prev': None,
            'opened_img': 0,
            'opened_pairs': 0,
            'buttons': [],
            'faq': None
            }


def end_game_modal(text, main_window):
    text = text + '\n Начать новую игру?'
    msg_box = messagebox.askyesno('Конец', text)
    if msg_box is False:
        main_window.destroy()
    else:
        main_window.destroy()
        settings = GameSet(0)
        Game(settings)


def start_game_modal():
    pass


class Game(object):
    def __init__(self, settings):
        self._settings = settings
        self.main_window = tkinter.Tk()
        self.main_window.title('Запоминалка')
        self.main_window.resizable(width=False, height=False)
        self._TimerFrame = tkinter.Frame(self.main_window)
        self._TimerFrame.grid(row=settings.SIDE+1, columnspan=settings.SIDE,
                              sticky=tkinter.W)
        self._timer = tkinter.Label(self._TimerFrame)
        self._timer.pack(fill=tkinter.X)
        self._settings.settings['buttons'] = self.buttons_set()
        self._settings.settings['faq'] = tkinter.PhotoImage(file='FAQ.gif')
        self.main_window.after(1500, self.hide,
                               self._settings.settings['buttons'])
        self.main_window.after(1000, self.game_timer)
        self.main_window.mainloop()

    def get_images(self):
        Q_SIDE = self._settings.settings['Q_SIDE']
        files = [os.path.join('gif', f) for f in os.listdir('gif')]
        shuffle(files)
        files = files[0:Q_SIDE]*2
        shuffle(files)
        images = [tkinter.PhotoImage(file=f) for f in files]
        return images, files

    def buttons_set(self):
        SIDE = self._settings.SIDE
        images, files = self.get_images()
        buttons = []
        for i in range(SIDE):
            for j in range(SIDE):
                btn = tkinter.Button(self.main_window,
                                     relief=tkinter.FLAT,
                                     image=images[SIDE*i + j]
                                     )
                btn.configure(command=lambda b=btn: self.change(b))
                btn.x = files[SIDE*i + j]
                btn.img = images[SIDE*i + j]
                btn.grid(row=i, column=j)
                buttons.append(btn)
        return buttons

    def change(self, btn):
        settings = self._settings.settings
        if btn['state'] not in ('normal', 'active'):
            return None

        settings['opened_img'] += 1
        if settings['opened_img'] <= 2:
            btn.configure(image=btn.img)
        else:
            return None
        if settings['prev'] is None:
            settings['prev'] = btn
        else:
            if settings['prev'].x != btn.x or settings['prev'] is btn:
                self.main_window.after(1000, self.hide,
                                       [settings['prev'], btn])
            elif settings['prev'].x == btn.x:
                settings['opened_pairs'] += 1
                btn.configure(state=tkinter.DISABLED)
                settings['prev'].configure(state=tkinter.DISABLED)
                settings['opened_img'] = 0
                self.win_condition()
            settings['prev'] = None

    def hide(self, list_btn):
        faq = self._settings.settings['faq']
        self._settings.settings['opened_img'] = 0
        for btn in list_btn:
            btn.configure(image=faq)

    def game_timer(self):
        if self._settings.settings['stop_time']:
            return None
        else:
            gmin = self._settings.settings['game_time'] // 60
            gsec = self._settings.settings['game_time'] % 60
            if gmin <= 0 and gsec <= 0:
                end_game_modal('Вы проиграли! Время истекло!',
                               self.main_window)
            else:
                if gmin < 10:
                    gmin = '0{0:s}'.format(str(gmin))
                else:
                    gmin = str(gmin)
                if gsec < 10:
                    gsec = '0{0:s}'.format(str(gsec))
                else:
                    gsec = str(gsec)
                    self._timer.configure(text='До конца игры осталось \
                    {0:s}:{1:s}'.format(gmin, gsec))
                self._settings.settings['game_time'] -= 1
                self.main_window.after(1000, self.game_timer)

    def win_condition(self):
        current = self._settings.settings['opened_pairs']
        total = self._settings.settings['Q_SIDE']
        if current == total:
            self._settings.settings['stop_time'] = True
            end_game_modal('Вы победили! Всё каритнки открыты!',
                           self.main_window)


if __name__ == '__main__':
    settings = GameSet(0)
    Game(settings)
