import sys
from cx_Freeze import setup, Executable


# Dependencies are automatically detected, but it might need
# fine tuning.
excludes = ['unicodedata', 'logging', 'unittest', 'email', 'html', 'http',
            'urllib', 'xml', 'bz2', 'pydoc']

buildOptions = dict(packages=[], excludes=excludes,
                    include_files=[('gif/', 'gif/'), ('FAQ.gif', 'FAQ.gif')]
                    )

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

executables = [
    Executable('game.py', base=base, targetName='Memorize')
]

setup(name='memorize',
      version='1.0',
      description='',
      options=dict(build_exe=buildOptions),
      executables=executables)
